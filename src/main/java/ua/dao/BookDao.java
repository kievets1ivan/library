package ua.dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import ua.models.Book;

import java.util.List;

public class BookDao implements IBookDao<Book> {

    public BookDao() { }

    private Session currentSession;
    private Transaction currentTransaction;

    public Session openCurrentSession() {
        currentSession = getSessionFactory().openSession();
        return currentSession;
    }
    public Session openCurrentSessionwithTransaction() {
        currentSession = getSessionFactory().openSession();
        currentTransaction = currentSession.beginTransaction();
        return currentSession;
    }
    public void closeCurrentSession() {
        currentSession.close();
    }
    public void closeCurrentSessionwithTransaction() {
        currentTransaction.commit();
        currentSession.close();
    }
    private static SessionFactory getSessionFactory() {
        Configuration configuration = new Configuration().configure();
        StandardServiceRegistryBuilder builder = new StandardServiceRegistryBuilder()
                .applySettings(configuration.getProperties());
        SessionFactory sessionFactory = configuration.buildSessionFactory(builder.build());
        return sessionFactory;
    }
    public Session getCurrentSession() {
        return currentSession;
    }

    public void add(Book book) {
        getCurrentSession().save(book);
    }
    public void update(Book book) {
        getCurrentSession().update(book);
    }

    public Book findById(int bookId) {
        Book book = (Book) getCurrentSession().get(Book.class, bookId);
        return book;
    }
    public void delete(Book book) {
        getCurrentSession().delete(book);
    }

    @SuppressWarnings("unchecked")
    public List<Book> findAll() {
       List<Book> books = (List<Book>) getCurrentSession().createQuery("from Book").list();
        return books;
    }

    public void deleteAll() {
        getCurrentSession().createQuery("delete from Book").executeUpdate();
    }
}