package ua.dao;

import org.hibernate.Session;

import java.util.List;

public interface IBookDao<T> {

    Session openCurrentSessionwithTransaction();
    void closeCurrentSessionwithTransaction();
    void closeCurrentSession();
    Session openCurrentSession();

    void add(T book);
    void update(T book);
    T findById(int id);
    void delete(T book);
    List<T> findAll();
    void deleteAll();
}
