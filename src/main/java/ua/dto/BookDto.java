package ua.dto;

import java.sql.Date;

public class BookDto {

    private int id;
    private String genre;
    private String author;
    private String title;
    private String edition;
    private int isbn;
    private int pages;
    private int year;
    private String language;
    private Date dateOfAddToDb;

    public BookDto() { }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getEdition() {
        return edition;
    }

    public void setEdition(String edition) {
        this.edition = edition;
    }

    public int getIsbn() {
        return isbn;
    }

    public void setIsbn(int isbn) {
        this.isbn = isbn;
    }

    public int getPages() {
        return pages;
    }

    public void setPages(int pages) {
        this.pages = pages;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public Date getDateOfAddToDb() {
        return dateOfAddToDb;
    }

    public void setDateOfAddToDb(Date dateOfAddToDb) {
        this.dateOfAddToDb = dateOfAddToDb;
    }
}
