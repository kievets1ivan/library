package ua.models;

import javax.persistence.*;
import java.sql.Date;

@Entity
@Table(name = "Books")
public class Book {

    @Id
    @GeneratedValue
    @Column(name = "Id")
    private int id;
    @Column(name = "Genre")
    private String genre;
    @Column(name = "Author")
    private String author;
    @Column(name = "Title")
    private String title;
    @Column(name = "Edition")
    private String edition;
    @Column(name = "ISBN")
    private int isbn;
    @Column(name = "Pages")
    private int pages;
    @Column(name = "PublicationYear")
    private int year;
    @Column(name = "Language")
    private String language;
    @Column(name = "DateOfAddToDb")
    private Date dateOfAddToDb;

    public Book() {
    }

    public Book(int id, String genre, String author, String title, String edition, int isbn, int pages, int year, String language, Date dateOfAddToDb) {
        this.id = id;
        this.genre = genre;
        this.author = author;
        this.title = title;
        this.edition = edition;
        this.isbn = isbn;
        this.pages = pages;
        this.year = year;
        this.language = language;
        this.dateOfAddToDb = dateOfAddToDb;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getEdition() {
        return edition;
    }

    public void setEdition(String edition) {
        this.edition = edition;
    }

    public int getIsbn() {
        return isbn;
    }

    public void setIsbn(int isbn) {
        this.isbn = isbn;
    }

    public int getPages() {
        return pages;
    }

    public void setPages(int pages) {
        this.pages = pages;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public Date getDateOfAddToDb() {
        return dateOfAddToDb;
    }

    public void setDateOfAddToDb(Date dateOfAddToDb) {
        this.dateOfAddToDb = dateOfAddToDb;
    }
}
