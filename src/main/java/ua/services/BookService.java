package ua.services;

import ua.dao.BookDao;
import ua.dao.IBookDao;
import ua.models.Book;

import java.util.List;

public class BookService {

    private static IBookDao bookDao;

    public BookService() {
        this.bookDao = new BookDao();
    }

    public void add(Book book) {
        bookDao.openCurrentSessionwithTransaction();
        bookDao.add(book);
        bookDao.closeCurrentSessionwithTransaction();
    }
    public void update(Book book)  {
        bookDao.openCurrentSessionwithTransaction();
        bookDao.update(book);
        bookDao.closeCurrentSessionwithTransaction();
    }
    public Book findById(int id) {
        bookDao.openCurrentSession();
        Book book = (Book) bookDao.findById(id);
        bookDao.closeCurrentSession();

        return book;
    }
    public void delete(Book book) {
        bookDao.openCurrentSessionwithTransaction();
        bookDao.delete(book);
        bookDao.closeCurrentSessionwithTransaction();

    }
    public List<Book> findAll() {
        bookDao.openCurrentSession();
        List<Book> books = bookDao.findAll();
        bookDao.closeCurrentSession();

        return books;
    }
    public void deleteAll() {
        bookDao.openCurrentSessionwithTransaction();
        bookDao.deleteAll();
        bookDao.closeCurrentSessionwithTransaction();
    }
}
