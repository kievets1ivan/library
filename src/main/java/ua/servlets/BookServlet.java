package ua.servlets;

import ua.dto.BookDto;
import ua.models.Book;
import ua.services.BookService;
import ua.utill.Converter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Date;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@WebServlet(urlPatterns = "/books")
public class BookServlet extends HttpServlet {

    private static String INSERT_OR_EDIT = "/insert.jsp";
    private static String SHOW_ALL = "/showAll.jsp";
    private static String ERROR = "/error.jsp";
    private BookService bookService;

    public BookServlet() {
        this.bookService = new BookService();
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String page = ERROR;

        try {

            String action = request.getParameter("action");

            if (action == null || action.equalsIgnoreCase("getAll")) {

                request.setAttribute("books", GetBooks());
                page = SHOW_ALL;
            } else if (action.equalsIgnoreCase("delete")) {

                Book book = bookService.findById(Integer.parseInt(request.getParameter("bookId")));
                bookService.delete(book);

                request.setAttribute("books", GetBooks());
                page = SHOW_ALL;
            } else if (action.equalsIgnoreCase("deleteAll")) {

                bookService.deleteAll();
                request.setAttribute("books", GetBooks());
                page = SHOW_ALL;
            } else if (action.equalsIgnoreCase("edit")) {

                Book book = bookService.findById(Integer.parseInt(request.getParameter("bookId")));
                request.setAttribute("book", book);

                page = INSERT_OR_EDIT;
            } else {
                page = INSERT_OR_EDIT;
            }

        } catch (SQLException | ClassNotFoundException e) {
            throw new ServletException(e);
        } catch (Exception e) {
            request.setAttribute("message", e.getMessage());

            page = ERROR;
        }finally {
            request.getRequestDispatcher(page).forward(request, response);
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {

            BookDto bookDto = new BookDto();

            bookDto.setGenre(request.getParameter("genre"));
            bookDto.setAuthor(request.getParameter("author"));
            bookDto.setTitle(request.getParameter("title"));
            bookDto.setEdition(request.getParameter("edition"));
            bookDto.setIsbn(Integer.parseInt(request.getParameter("isbn")));
            bookDto.setPages(Integer.parseInt(request.getParameter("pages")));
            bookDto.setYear(Integer.parseInt(request.getParameter("year")));
            bookDto.setLanguage(request.getParameter("language"));
            bookDto.setDateOfAddToDb(Date.valueOf(request.getParameter("dateOfAddToDb")));

            String bookId = request.getParameter("bookId");
            if (bookId == null || bookId.isEmpty()) {

                bookService.add(Converter.BookDtoToBook(new Book(), bookDto));

            } else {
                bookDto.setId(Integer.parseInt(bookId));
                bookService.update(Converter.BookDtoToBook(new Book(), bookDto));
            }

        } catch (Exception e) {
            throw new ServletException(e);
        }

        doGet(request, response);

    }

    List<BookDto> GetBooks() throws Exception {
        List<BookDto> books = new ArrayList<>();
        List<Book> booksDb = bookService.findAll();

        for (Book book : booksDb) {
            books.add(Converter.BookToBookDto(new BookDto(), book));
        }

        if(books.isEmpty())
            throw new Exception("Library is empty!");
        return books;
    }
}
