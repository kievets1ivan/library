package ua.utill;

import ua.dto.BookDto;
import ua.models.Book;

public class Converter {

    public static BookDto BookToBookDto(BookDto bookDto, Book book){
        bookDto.setId(book.getId());
        bookDto.setGenre(book.getGenre());
        bookDto.setAuthor(book.getAuthor());
        bookDto.setTitle(book.getTitle());
        bookDto.setEdition(book.getEdition());
        bookDto.setIsbn(book.getIsbn());
        bookDto.setPages(book.getPages());
        bookDto.setYear(book.getYear());
        bookDto.setLanguage(book.getLanguage());
        bookDto.setDateOfAddToDb(book.getDateOfAddToDb());

        return  bookDto;
    }
    public static Book BookDtoToBook(Book book, BookDto bookDto){
        book.setId(bookDto.getId());
        book.setGenre(bookDto.getGenre());
        book.setAuthor(bookDto.getAuthor());
        book.setTitle(bookDto.getTitle());
        book.setEdition(bookDto.getEdition());
        book.setIsbn(bookDto.getIsbn());
        book.setPages(bookDto.getPages());
        book.setYear(bookDto.getYear());
        book.setLanguage(bookDto.getLanguage());
        book.setDateOfAddToDb(bookDto.getDateOfAddToDb());

        return book;
    }
}

