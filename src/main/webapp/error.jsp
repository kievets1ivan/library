<%@ page language="java" %>
<%@ page contentType="text/html;charset=utf-8" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Error</title>
    <link href="css/pen.css" rel="stylesheet" type="text/css">
    <link href="css/fontawesome-free-5.13.0-web/css/all.min.css" rel="stylesheet" type="text/css">
    <style rel="stylesheet" type="text/css">
        p{
            text-align: center;
            font-size: 24px;
        }

        .con{
            display: flex;
            justify-content: center;
            vertical-align: middle;
        }
        .err{
            padding: 10px;
            font-size: 4rem;
            color: red;
        }

    </style>
</head>
<body>
<div class="header">
    <h1 style="color: red">Помилка</h1>
    <a href="main.html"><i id="toMain" class="fas fa-home"></i></a>
</div>

<div class="con">
    <i class="fas fa-times err"></i>
    <p>${message}</p>
</div>

<div class="footer">&copy; Kievets, Zubko</div>

</body>
</html>