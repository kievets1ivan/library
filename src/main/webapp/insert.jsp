<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset= UTF-8">
    <title>Add new book</title>
    <link href="css/pen.css" rel="stylesheet" type="text/css">
    <link href="css/fontawesome-free-5.13.0-web/css/all.min.css" rel="stylesheet" type="text/css">
</head>
<body>

<div class="header">
    <h1>Web-сторінка додавання запису до бази даних</h1>
    <a href="main.html"><i id="toMain" class="fas fa-home"></i></a>
</div>

<div class="form-style-6">
    <form method="post" action="books">
        <!--id must be commented
        Book ID :--> <input type="hidden" name="bookId" value="<c:out value="${book.id}" />"/> <br/>
        Genre : <input required  pattern="[A-Za-z]{6,25}" title="Length 6-25 symbols" type="text" name="genre" value="<c:out value="${book.genre}" />"/> <br/>
        Title : <input required  pattern="[A-Za-z]{3,25}" title="Length 3-25 symbols" type="text" name="title" value="<c:out value="${book.title}" />"/> <br/>
        Author : <input required  pattern="[A-Za-z]{6,25}" title="Length 6-25 symbols" type="text" name="author" value="<c:out value="${book.author}" />"/> <br/>
        Edition : <input required  pattern="[A-Za-z]{3,25}" title="Length 3-25 symbols"type="text" name="edition" value="<c:out value="${book.edition}" />"/> <br/>
        ISBN : <input required  pattern="[0-9]{5}" title="Only 5 numbers" type="text" name="isbn" value="<c:out value="${book.isbn}" />"/> <br/>
        Pages : <input required  pattern="[0-9]{2,3}" title="Only 2-3 numbers" type="text" name="pages" value="<c:out value="${book.pages}" />"/> <br/>
        Publication year : <input required  type="number" min="1" max="2020" title="Till 2020" name="year" value="<c:out value="${book.year}" />"/> <br/>
        Language : <select id="language" name="language" >
                    <option value="Ukrainian">Ukrainian</option>
                    <option value="Russian">Russian</option>
                    <option value="English">English</option>
                    </select> <br/>
        Date of adding to DB : <input required  type="date" max="2020-05-23" name="dateOfAddToDb" value="<c:out value="${book.dateOfAddToDb}" />"/> <br/>

        <input class="mar" type="submit" value="Submit">
        <input type="reset" value="Reset">
    </form>
</div>

<div class="footer">&copy; Kievets, Zubko</div>

</body>
</html>