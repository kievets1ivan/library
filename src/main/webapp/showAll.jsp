<%@ page language="java" %>
<%@ page contentType="text/html;charset=utf-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>
    <meta http-equiv="Content-Type" content="text/html" charset="UTF-8">
    <link href="css/pen.css" rel="stylesheet" type="text/css">
    <link href="css/fontawesome-free-5.13.0-web/css/all.min.css" rel="stylesheet" type="text/css">
    <title>Show All Users</title>
</head>
<body>

<div class="header">
    <h1>Web-сторінка з даними</h1>
    <a href="main.html"><i id="toMain" class="fas fa-home"></i></a>
</div>

<table>
    <thead>
    <tr>
        <th colspan="12">Library</th>
    </tr>
    <tr>
        <th>Book Id</th>
        <th>Genre</th>
        <th>Title</th>
        <th>Author</th>
        <th>Edition</th>
        <th>ISBN</th>
        <th>Pages</th>
        <th>Publication year</th>
        <th>Language</th>
        <th>Date of adding to DB</th>
        <th colspan=2>Action</th>
    </tr>
    </thead>
    <tbody>
    <c:forEach items="${books}" var="book">
        <tr>
            <td>${book.id}</td>
            <td>${book.genre}</td>
            <td>${book.title}</td>
            <td>${book.author}</td>
            <td>${book.edition}</td>
            <td>${book.isbn}</td>
            <td>${book.pages}</td>
            <td>${book.year}</td>
            <td>${book.language}</td>
            <td>${book.dateOfAddToDb}</td>
            <td>
                <a href="books?action=edit&bookId=<c:out value="${book.id}"/>"><i
                        class="fas fa-pen button edit"></i></a>
            </td>
            <td>
                <a href="books?action=delete&bookId=<c:out value="${book.id}"/>"><i
                        class="fas fa-trash button delete"></i></a>
            </td>
        </tr>
    </c:forEach>
    </tbody>
</table>

<div class="pre-row">
    <div class="row">
        <a class="interact buttonGreen" href="insert.jsp">Add book</a>
        <a class="interact buttonRed" href="books?action=deleteAll">Delete all library</a>
        <a class="interact buttonBlue" href="main.html">To main page</a>
    </div>
</div>

<div class="footer">&copy; Kievets, Zubko</div>

</body>
</html>